package desafioconcrete.com.githubjavapop.Domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by rafael on 04/12/16.
 */
public class Pull implements Serializable {

    private String title;
    private String body;
    private User user;
    private String created_at;

    public String getTitle() {
        return title;
    }



    public String getDescription() {
        return body;
    }


    public User getUser() {
        return user;
    }


    public String getCreated_at() {
        return created_at;
    }

}
