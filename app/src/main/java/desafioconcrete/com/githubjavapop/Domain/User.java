package desafioconcrete.com.githubjavapop.Domain;

import java.io.Serializable;

/**
 * Created by rafael on 04/12/16.
 */
public class User implements Serializable{

    private String login;
    private String avatar_url;

    public String getLogin() {
        return login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}
