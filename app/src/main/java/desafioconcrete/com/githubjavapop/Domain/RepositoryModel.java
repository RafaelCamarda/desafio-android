package desafioconcrete.com.githubjavapop.Domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rafael on 03/12/16.
 */
public class RepositoryModel implements Serializable {

    private List<Repository> items;

    public List<Repository> getItems() {
        return items;
    }

}
