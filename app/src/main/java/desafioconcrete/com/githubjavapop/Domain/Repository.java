package desafioconcrete.com.githubjavapop.Domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by rafael on 03/12/16.
 */
public class Repository implements Serializable {

    private String name;
    private String description;
    private int forks_count;
    private int stargazers_count;

    private Owner owner;

    public String getName() {
        return name;
    }


    public String getDescription() {
        return description;
    }


    public int getForks_count() {
        return forks_count;
    }


    public int getStargazers_count() {
        return stargazers_count;
    }

    public Owner getOwner() {
        return owner;
    }

    public Repository(String name) {
        this.name = name;
    }



}
