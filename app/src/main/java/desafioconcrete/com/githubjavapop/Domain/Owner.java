package desafioconcrete.com.githubjavapop.Domain;

import java.io.Serializable;

/**
 * Created by rafael on 03/12/16.
 */
public class Owner implements Serializable {

    private String login;
    private String avatar_url;

    public String getLogin() {
        return login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

}
