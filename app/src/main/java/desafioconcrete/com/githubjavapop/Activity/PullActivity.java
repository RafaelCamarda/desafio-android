package desafioconcrete.com.githubjavapop.Activity;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import desafioconcrete.com.githubjavapop.API.GitHubClient;
import desafioconcrete.com.githubjavapop.API.ServiceGenerator;
import desafioconcrete.com.githubjavapop.Adapters.PullsAdapter;
import desafioconcrete.com.githubjavapop.Domain.Pull;
import desafioconcrete.com.githubjavapop.R;
import retrofit2.Call;

/**
 * Created by rafael on 03/12/16.
 */

@EActivity(R.layout.activity_pulls)
public class PullActivity extends AppCompatActivity {

    //Repository Data
    @Extra("ownerName")
    String owner;

    @Extra("repoName")
    String repoName;


    private  LinearLayoutManager mLayoutManager =  new LinearLayoutManager(PullActivity.this);

    private List<Pull> pulls = new ArrayList<>();
    private PullsAdapter recyclerAdapter = null;

    @ViewById(R.id.tv_webError)
    TextView tvError;

    @ViewById(R.id.rv_git_pulls)
    RecyclerView rvGitPop;

    @ViewById(R.id.my_toolbar)
    Toolbar myToolbar;

    @ViewById(R.id.srl_swipe)
    SwipeRefreshLayout swipeRefresh;

    @AfterViews
    void init(){
        myToolbar.setTitle(repoName);
        setSupportActionBar(myToolbar);
        myToolbar.setTitleTextColor(ContextCompat.getColor(PullActivity.this, R.color.color_white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getPulls();
        setRefresher();
    }



    @Background
    public void getPulls(){

        GitHubClient client = ServiceGenerator.createService(GitHubClient.class);

        Call<List<Pull>> call = client.getPulls(owner,repoName);

        try {
            pulls = call.execute().body();

            if(pulls != null){
                recyclerAdapter = new PullsAdapter(pulls, PullActivity.this);

                //RECYCLERWEB
                initRecyclerView();
            }

            onItemsLoadComplete();
        }
        catch(IOException e){
            e.printStackTrace();
            networkConnectionError();

        }
    }

    @UiThread
    void networkConnectionError() {
        if(pulls.size() == 0){
            tvError.setVisibility(View.VISIBLE);
        }
        swipeRefresh.setRefreshing(false);
        Toast.makeText(PullActivity.this,R.string.erro_dados,
                Toast.LENGTH_SHORT).show();
    }

    @UiThread
    void initRecyclerView() {

        recyclerAdapter = new PullsAdapter(pulls,PullActivity.this);
        rvGitPop.setItemAnimator(new DefaultItemAnimator());
        rvGitPop.setAdapter(recyclerAdapter);
        rvGitPop.setLayoutManager(mLayoutManager);
        rvGitPop.setHasFixedSize(true);

    }

    @UiThread
        //Swipe to refresh
    void setRefresher() {
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                getPulls();
            }
        });

    }

    @UiThread
    void onItemsLoadComplete() {
        tvError.setVisibility(View.GONE);
        swipeRefresh.setRefreshing(false);
    }

}
