package desafioconcrete.com.githubjavapop.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import desafioconcrete.com.githubjavapop.API.GitHubClient;
import desafioconcrete.com.githubjavapop.API.ServiceGenerator;
import desafioconcrete.com.githubjavapop.Adapters.RepositoryAdapter;
import desafioconcrete.com.githubjavapop.Domain.Repository;
import desafioconcrete.com.githubjavapop.Domain.RepositoryModel;
import desafioconcrete.com.githubjavapop.R;
import retrofit2.Call;

/**
 * Created by rafael on 03/12/16.
 */



@EActivity(R.layout.activity_git_pop)
public class MainActivity extends AppCompatActivity {

    private int page = 1;
    private final String language = "language:Java";
    private final String sort = "stars";
    private  LinearLayoutManager mLayoutManager =  new LinearLayoutManager(MainActivity.this);
    private  RepositoryModel model = null;
    private List<Repository> repositories = new ArrayList<>();
    private RepositoryAdapter recyclerAdapter = null;


    @ViewById(R.id.tv_webError)
    TextView tvError;

    @ViewById(R.id.rv_git_pop)
    RecyclerView rvGitPop;

    @ViewById(R.id.srl_swipe)
    SwipeRefreshLayout swipeRefresh;

    @ViewById(R.id.my_toolbar)
    Toolbar myToolbar;

    @AfterViews
    void init(){

        myToolbar.setTitle(R.string.app_title);
        myToolbar.setTitleTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_white));
        setSupportActionBar(myToolbar);
        initRecyclerView();
        getRepositories(page);
        setRefresher();
    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Background
    public void getRepositories(int page){
        //add loading progress view
        if(page > 1)
            repositories.add(new Repository("load"));
        notifyItenInsertedUi();


        GitHubClient client = ServiceGenerator.createService(GitHubClient.class);

        Call<RepositoryModel> call = client.repositoryModel(language,sort,page);

        try {

            model = call.execute().body();


            if(model != null){
                if(page > 1)
                    repositories.remove(repositories.size()-1);

                List<Repository> result = model.getItems();

                if(result.size() > 0){
                    repositories.addAll(result);
                }
                else{
                    recyclerAdapter.setMoreDataAvailable(false);
                    //telling adapter to stop calling load more as no more server data available
                    Toast.makeText(MainActivity.this, R.string.fim_repositorios,
                            Toast.LENGTH_LONG).show();
                }

                recyclerAdapter.notifyDataChanged();
            }
            else{
                if(page > 1)
                    repositories.remove(repositories.size()-1);

                recyclerAdapter.setMoreDataAvailable(false);

                toastEndUI();
            }
            onItemsLoadComplete();
        }
        catch(IOException e){
            e.printStackTrace();
            networkConnectionError();

        }
    }

    @UiThread
    void toastEndUI() {
        Toast.makeText(MainActivity.this, R.string.fim_repositorios,
                Toast.LENGTH_LONG).show();
    }

    @UiThread
    void networkConnectionError() {
        if(page == 1 && repositories.size() == 0){
            tvError.setVisibility(View.VISIBLE);
        }
        swipeRefresh.setRefreshing(false);
        Toast.makeText(MainActivity.this, R.string.erro_dados,
                Toast.LENGTH_SHORT).show();
    }

    @UiThread
    void notifyItenInsertedUi() {
        recyclerAdapter.notifyItemInserted(repositories.size()-1);

    }

    @UiThread
    void initRecyclerView() {

        recyclerAdapter = new RepositoryAdapter(repositories,MainActivity.this);

        recyclerAdapter.setLoadMoreListener(new RepositoryAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                rvGitPop.post(new Runnable() {
                    @Override
                    public void run() {
                        int index = ++page;
                        getRepositories(index);// a method which requests remote data
                    }
                });

            }
        });


        rvGitPop.setItemAnimator(new DefaultItemAnimator());
        rvGitPop.setAdapter(recyclerAdapter);
        rvGitPop.setLayoutManager(mLayoutManager);
        rvGitPop.setHasFixedSize(true);



    }

    @UiThread
    //Swipe to refresh
     void setRefresher() {
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                getRepositories(page);
            }
        });

    }

    @UiThread
     void onItemsLoadComplete() {
        tvError.setVisibility(View.GONE);
        swipeRefresh.setRefreshing(false);
    }

    public void onItemClicked(String owner, String repoName){


        // You can even provide extras defined with @Extra in the activity
        PullActivity_.intent(MainActivity.this)
                .extra("ownerName",owner)
                .extra("repoName",repoName)
                .start();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_sobre:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                String text = "Desafio Concrete Github JavaPop\n\n"
                        +"Desenvolvido por: Rafael Camarda\n"+
                        "Contato: rf.camarda@gmail.com\n";

                alertDialogBuilder.setMessage(text);

                alertDialogBuilder.show();

                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

}
