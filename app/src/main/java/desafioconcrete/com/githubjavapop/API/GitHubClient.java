package desafioconcrete.com.githubjavapop.API;


import java.util.List;

import desafioconcrete.com.githubjavapop.Domain.Pull;
import desafioconcrete.com.githubjavapop.Domain.RepositoryModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by rafael on 03/12/16.
 */
public interface GitHubClient {
    @GET("search/repositories")
    Call<RepositoryModel> repositoryModel(
            @Query("q") String language,
            @Query("sort") String stars,
            @Query("page") int page
    );

    @GET("repos/{owner}/{repository_name}/pulls")
    Call<List<Pull>>getPulls
            (@Path("owner") String owner,
             @Path("repository_name") String repositoryName);
}
