package desafioconcrete.com.githubjavapop.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import java.util.List;

import desafioconcrete.com.githubjavapop.Domain.Pull;
import desafioconcrete.com.githubjavapop.R;

/**
 * Created by rafael on 04/12/16.
 */
public class PullsAdapter extends RecyclerView.Adapter<PullsViewHolder> {

    private List<Pull> pulls = null;
    private Context context;


    public PullsAdapter(List<Pull> pulls, Context context) {
        this.pulls = pulls;
        this.context = context;
    }


    @Override
    public PullsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new PullsViewHolder(inflater.inflate(R.layout.item_pull, parent, false), context);
    }

    @Override
    public void onBindViewHolder(PullsViewHolder holder, int position) {
        Pull pullBinded = pulls.get(position);

        holder.pullName.setText(pullBinded.getTitle());
        holder.pullDesc.setText(pullBinded.getDescription());
        holder.userName.setText(pullBinded.getUser().getLogin());
        holder.pullData.setText(formatDate(pullBinded.getCreated_at()));

        //Picasso with disk cache
        new Picasso.Builder(context)
                .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                .build()
                .load(pullBinded.getUser()
                        .getAvatar_url())
                .placeholder(R.drawable.ic_user)
                .into(holder.userPic);
    }

    @Override
    public int getItemCount() {
        return pulls.size();
    }

    public String formatDate(String date) {

        if(date != null){
            String day = date.substring(8,10);
            String month = date.substring(5,7);
            String year = date.substring(0,4);

            return day+"/"+month+"/"+year;
        }
        else{
            return "";
        }

    }
}
