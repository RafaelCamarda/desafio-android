package desafioconcrete.com.githubjavapop.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.UiThread;

import java.util.List;

import desafioconcrete.com.githubjavapop.Activity.MainActivity;
import desafioconcrete.com.githubjavapop.Domain.Repository;
import desafioconcrete.com.githubjavapop.R;

/**
 * Created by rafael on 03/12/16.
 */
public class RepositoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public final int TYPE_REPO = 0;
    public final int TYPE_LOAD = 1;

    private List<Repository> repositories = null;
    private Context context;
    private MainActivity main = null;
    volatile boolean  isLoading = false, isMoreDataAvailable = true;
    OnLoadMoreListener loadMoreListener;

    public RepositoryAdapter(List<Repository> repositories, Context context){
        this.repositories = repositories;
        this.context = context;
        main = (MainActivity) context;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        if(viewType==TYPE_REPO){
            return new RepositoryViewHolder(inflater.inflate(R.layout.item_repository,parent,false)
            ,context);
        }else{
            return new LoadHolder(inflater.inflate(R.layout.item_load,parent,false));
        }
    }

    static class LoadHolder extends RecyclerView.ViewHolder{
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Repository repoBinded = repositories.get(position);


        if(position >= getItemCount()-1 && isMoreDataAvailable &&
                !isLoading && loadMoreListener!=null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if(getItemViewType(position)==TYPE_REPO){
            ((RepositoryViewHolder)holder).repoName.setText(repoBinded.getName());
            ((RepositoryViewHolder)holder).repoDesc.setText(repoBinded.getDescription());
            ((RepositoryViewHolder)holder).forksQtd.setText(Integer.toString(repoBinded.getForks_count()));
            ((RepositoryViewHolder)holder).starsQtd.setText(Integer.toString(repoBinded.getStargazers_count()));
            ((RepositoryViewHolder)holder).ownerName.setText(repoBinded.getOwner().getLogin());

            //Picasso with disk cache
            new Picasso.Builder(context)
                    .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                    .build()
                    .load(repoBinded.getOwner()
                    .getAvatar_url())
                    .placeholder(R.drawable.ic_user)
                    .into(((RepositoryViewHolder)holder).ownerPic);
        }



    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(!(repositories.get(position).getName().equals("load"))){
            return TYPE_REPO;
        }else{
            return TYPE_LOAD;
        }
    }


    //LoadMore


    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */

    public void notifyDataChanged(){

        main.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
                isLoading = false;
            }
        });


    }


    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}
