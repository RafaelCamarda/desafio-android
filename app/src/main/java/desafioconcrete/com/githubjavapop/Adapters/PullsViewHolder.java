package desafioconcrete.com.githubjavapop.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import desafioconcrete.com.githubjavapop.R;

/**
 * Created by rafael on 04/12/16.
 */
public class PullsViewHolder extends RecyclerView.ViewHolder{

    public TextView pullName;
    public TextView pullDesc;
    public TextView pullData;
    public TextView userName;
    public ImageView userPic;

    private Context context;

    public PullsViewHolder(View itemView, final Context context) {
        super(itemView);
        this.context = context;

        pullName = (TextView) itemView.findViewById(R.id.tv_pull_name);
        pullDesc = (TextView) itemView.findViewById(R.id.tv_pull_desc);
        pullData = (TextView) itemView.findViewById(R.id.tv_pull_data);
        userName = (TextView) itemView.findViewById(R.id.tv_user_name);
        userPic = (ImageView) itemView.findViewById(R.id.iv_user_pic);
    }
}
