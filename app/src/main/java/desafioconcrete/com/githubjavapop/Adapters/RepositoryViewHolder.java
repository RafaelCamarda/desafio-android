package desafioconcrete.com.githubjavapop.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.ViewsById;

import desafioconcrete.com.githubjavapop.Activity.MainActivity;
import desafioconcrete.com.githubjavapop.R;

/**
 * Created by rafael on 03/12/16.
 */
public class RepositoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView repoName;
    public TextView repoDesc;
    public TextView forksQtd;
    public TextView starsQtd;
    public TextView ownerName;
    public ImageView ownerPic;
    private MainActivity main;

    private Context context;

    public RepositoryViewHolder(final View itemView, final Context context) {
        super(itemView);
        this.context = context;
        main = (MainActivity) context;
        itemView.setOnClickListener(this);

        repoName = (TextView) itemView.findViewById(R.id.tv_repo_name);
        repoDesc = (TextView) itemView.findViewById(R.id.tv_repo_desc);
        forksQtd = (TextView) itemView.findViewById(R.id.tv_fork_count);
        starsQtd = (TextView) itemView.findViewById(R.id.tv_star_count);
        ownerName = (TextView) itemView.findViewById(R.id.tv_owner_name);
        ownerPic = (ImageView) itemView.findViewById(R.id.iv_owner_pic);

    }

    @Override
    public void onClick(View view) {
        main.onItemClicked(ownerName.getText().toString(),repoName.getText().toString());
    }
}